import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import zipfile
import flask
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
from flask import Flask, render_template, Response,  request, session, redirect, url_for, send_from_directory, flash
from werkzeug.utils import secure_filename

from object_detection.utils import label_map_util

from object_detection.utils import visualization_utils as vis_util

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")


MODEL_NAME = 'new_model'  
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

PATH_TO_LABELS = os.path.join('training', 'object-detection.pbtxt')  

NUM_CLASSES = 10  



detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')


label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)




def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)

app = Flask(__name__)
UPLOAD_FOLDER = '/home/sunbeam/Desktop/final_project/object_detection/static/uploads'


  
def fun(filepath, filename):



# for i in os.listdir(f"{a}"):
    for i in os.listdir("/home/sunbeam/Desktop/final_project/object_detection/static/uploads"):
        ip = os.path.join(f"/home/sunbeam/Desktop/final_project/object_detection/static/uploads", i)
        image = Image.open(ip)
        nimg = image.resize((256,256))
        #     fp = os.path.join(op, ip)
        nimg.save(f"/home/sunbeam/Desktop/final_project/object_detection/static/uploads/"+i)

    PATH_TO_TEST_IMAGES_DIR = "/home/sunbeam/Desktop/final_project/object_detection/static/uploads"
    image_path = filepath
    IMAGE_SIZE = (12, 8)


    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            # i = 0
            # for image_path in TEST_IMAGE_PATHS:
                image = Image.open(image_path)
                # the array based representation of the image will be used later in order to prepare the
                # result image with boxes and labels on it.
                image_np = load_image_into_numpy_array(image)
                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
                image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                # Each box represents a part of the image where a particular object was detected.
                boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                # Each score represent how level of confidence for each of the objects.
                # Score is shown on the result image, together with the class label.
                scores = detection_graph.get_tensor_by_name('detection_scores:0')
                classes = detection_graph.get_tensor_by_name('detection_classes:0')
                num_detections = detection_graph.get_tensor_by_name('num_detections:0')
                # Actual detection.
                (boxes, scores, classes, num_detections) = sess.run(
                    [boxes, scores, classes, num_detections],
                    feed_dict={image_tensor: image_np_expanded})
                # print(classes)
                #print(classes)
                calories = {"apple":52,"sandwich":304,"cake":195,"french fries":274, "ice cream":196, "omellete":154, "red velvet cake":367, "donut":452, "garlic bread": 149,"mac and cheese":192}
                items =[category_index.get(value) for index,value in enumerate(classes[0]) if scores[0,index] > 0.5]
                # cal_count = ''
                # for d in items:
                # 	for key in d:
                # 		if d[key] in calories.keys():
                # 			print(f"You are having {d[key]} and it contains approximately {calories[d[key]]}  calories per 100 gm")
                			# cal_count = np.array(new_str)
                # Visualization of the results of a detection.
                vis_util.visualize_boxes_and_labels_on_image_array(
                    image_np,
                    np.squeeze(boxes),
                    np.squeeze(classes).astype(np.int32),
                    np.squeeze(scores),
                    # np.squeeze(cal_count),
                    category_index,
                    use_normalized_coordinates=True,
                    line_thickness=6)

                plt.figure(figsize=IMAGE_SIZE)
                plt.imshow(image_np)    # matplotlib is configured for command line only so we save the outputs instead
                # plt.savefig("/home/sunbeam/Desktop/final_project/object_detection/static/detections/detection_output{}.png".format(i))  # create an outputs folder for the images to be saved
                plt.savefig("/home/sunbeam/Desktop/final_project/object_detection/static/detections/{}".format(filename))


                items =[category_index.get(value) for index,value in enumerate(classes[0]) if scores[0,index] > 0.5]
                # cal_count = ''
                for d in items:
                    for key in d:
                        if d[key] in calories.keys():
                            return (f"You are having {d[key]} and it contains approximately {calories[d[key]]}  calories per 100 gm")
                
                # if items[0] in calories.keys():
                # 	print(items, calories[items[0]])

                # i = i+1  



app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER 
@app.route("/")
def index():
  return render_template("index.html")

# try:
#   a=input("enter the desired path from which you want to import the image \t")
# except:
#   a=input("please enter the valid path")

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      # create a secure filename
      filename = secure_filename(f.filename)
      print(filename)
      # save file to /static/uploads
      filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
      print(filepath)
      f.save(filepath)
      a = fun(filepath, filename)
      # get_image(filepath, filename)
      
      return render_template("uploaded.html", display_detection = filename, fname = filename, calories= a)





if __name__ == '__main__':


   app.run(port=3000, debug=True)